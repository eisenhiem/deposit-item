<?php

/* @var $this \yii\web\View */
/* @var $content string */


use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\Icon;

Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column min-vh-100">
<?php $this->beginBody() ?>

<header>
    <?php
        NavBar::begin([
            'brandLabel' => Icon::show('box') . 'โปรกรมเบิกพัสดุ',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md navbar-blue bg-light fixed-top',
            ],
        ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'encodeLabels' => false,
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'รายการขอเบิก', 'url' => ['/bills/index']],
            ['label' => 'ตั้งค่า', 'visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => 'ตั้งค่าแผนก/ฝ่าย','visible' => !Yii::$app->user->isGuest, 'url' => ['/departments/index']],
                ['label' => 'ตั้งค่าเจ้าหน้าที่','visible' => !Yii::$app->user->isGuest, 'url' => ['/officers/index']],
                ['label' => 'ตั้งค่าหน่วยงาน','visible' => !Yii::$app->user->isGuest, 'url' => ['/office/index']],    
                ['label' => 'ตั้งค่ารายการเบิก','visible' => !Yii::$app->user->isGuest, 'url' => ['/item/index']],    
            ]],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    </header>
    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer class="footer mt-auto py-3 text-muted">
        <div class="container">
            <p class="float-left">&copy; นายจักรพงษ์ วงศ์กมลาไสย <?= Icon::show('envelope') ?> e-mail : jwongkamalasai@gmail.com <?= date('Y') ?></p>
            <p class="float-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
