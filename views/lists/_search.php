<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ListsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lists-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'list_id') ?>

    <?= $form->field($model, 'bill_id') ?>

    <?= $form->field($model, 'list_item') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'cut_off') ?>

    <?php // echo $form->field($model, 'cut_off_date') ?>

    <?php // echo $form->field($model, 'remain') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
