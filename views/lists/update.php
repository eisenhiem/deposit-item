<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lists */

$this->title = 'แก้ไขรายการ: ' . $model->list_id;
$this->params['breadcrumbs'][] = ['label' => 'รายการ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->list_id, 'url' => ['view', 'id' => $model->list_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lists-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
