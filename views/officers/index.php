<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OfficersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เจ้าหน้าที่';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="officers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มเจ้าหน้าที่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'officer_id',
            'officer_name',
            'officer_position',
            'officer_level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
