<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Officers */

$this->title = 'เพิ่มเจ้าหน้าที่';
$this->params['breadcrumbs'][] = ['label' => 'เจ้าหน้าที่', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="officers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
