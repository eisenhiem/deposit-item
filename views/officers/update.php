<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Officers */

$this->title = 'แก้ไขข้อมูลเจ้าหน้าที่: ' . $model->officer_id;
$this->params['breadcrumbs'][] = ['label' => 'เจ้าหน้าที่', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->officer_id, 'url' => ['view', 'id' => $model->officer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="officers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
