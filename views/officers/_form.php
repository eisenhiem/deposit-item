<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Officers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="officers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'officer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'officer_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'officer_level')->dropDownList([ 3 => 'หัวหน้าเจ้าหน้าที่', 2 => 'ผู้ตรวจสอบ', 1 => 'เจ้าหน้าที่พัสดุ', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
