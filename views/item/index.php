<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการวัสดุ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรายการ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'item_id',
            'item_name',
            'item_type',
            'item_unit',
            'item_stock',
            //'item_incharge',
            //'item_avaliable',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:180px;'],
                'buttonOptions'=>['class'=>'btn btn-warning'],
                'template'=>'{update} {active} {delete}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return (Html::a(Icon::show('edit'),['update','id'=>$model->item_id],['class' => 'btn btn-warning']));
                    },
                    'active' => function($url,$model,$key){
                        return $model->item_avaliable == 1 ? (Html::a(Icon::show('check'),['change','id'=>$model->item_id],['class' => 'btn btn-success'])):(Html::a(Icon::show('times'),['change','id'=>$model->item_id],['class' => 'btn btn-danger'])) ;
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a(Icon::show('trash'), ['delete', 'id' => $model->item_id], [
                            'class' => 'btn btn-danger',
                            'style' => ['width' => '44px'],
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
