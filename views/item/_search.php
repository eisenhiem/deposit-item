<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'item_name')->label(false); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'item_avaliable')->radioList(['1' => 'เปิดให้เบิก', '0' => 'ไม่เปิดให้เบิก'])->label(false); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'item_incharge')->radioList(['watchara' => 'คลังวมย.', 'veeraya' => 'คลังพัสดุ'])->label(false); ?>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>