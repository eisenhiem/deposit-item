<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\bills */

$this->title = $model->bill_id;
$this->params['breadcrumbs'][] = ['label' => 'รายการใบคำขอเบิก', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bills-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= ($model->bill_status_id == '1' && Yii::$app->user->identity->id =='3') ? (Html::a('อนุมัติเบิกจ่าย', ['approve', 'id' => $model->bill_id], ['class' => 'btn btn-primary'])):''; ?>
        <?=  Html::a('พิมพ์ใบคำขอเบิก', ['print', 'id' => $model->bill_id], ['class' => 'btn btn-info']); ?>
    </p>

    <br>
    <p>
    <div> <b>เลขที่ใบคำขอเบิก </b>: <?= $model->bill_no; ?> </div>  
    <div> <b>กลุ่มงาน/ฝ่าย/แผนกที่ขอเบิก </b>: <?= $model->getDepName(); ?> </div>  
    <div> <b>วันที่ขอเบิก </b>: <?= Yii::$app->formatter->asDate($model->bill_date, 'dd/MM/yyyy'); ?> </div>  
    <div> <b>ผู้ขอเบิก </b>: <?= $model->bill_register; ?> </div>  
    <div> <b>เจ้าหน้าที่ </b>: <?= $model->getOfficerName(); ?> </div>  
    <div> <b>หัวหน้าเจ้าหน้าที่ </b>: <?= $model->getHeadName(); ?> </div>  
    
    <div> <b>สถานะใบคำขอเบิก </b>: <?= $model->getStatusName(); ?> </div> 
    <br>
    </p>
    <?= GridView::widget([
        //'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            'list_item',
            'amount',
            'cut_off',
            'cut_off_date',
            'remain',
            'comment:ntext',
        ],
    ]) ?>

</div>
