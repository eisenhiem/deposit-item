<?php
use yii\helpers\Html;

function changeDate($date){
//ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
  if(is_null($date)){
    return '...............................';
  } else {
    $get_date = explode("-",$date);
    //กำหนดชื่อเดือนใส่ตัวแปร $month
    $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    //month
    $get_month = $get_date["1"];
    //year    
    $year = $get_date["0"]+543;
    return $get_date["2"]." ".$month[$get_month]." พ.ศ. ".$year;
  }
}

function checkRequire($data){
  if(is_null($data)){
    return '..............................................';
  } else {
    return $data;
  }
}
//การเรียกใช้งาน Function

?>
<h3 align="center">ใบเบิกพัสดุ <?= $office->office_name;?></h3>
<div align="right"> เลขที่ <?= $model->bill_no;?> &emsp;&emsp;</div>
<div> &emsp;<?= $model->getDepName();?></div>
<div> &emsp;วันที่ขอเบิก <?= changeDate($model->bill_date);?></div>

<p>
    <table class="table_bordered" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th width=50 rowspan="2">ลำดับ</th>
            <th width=200 rowspan="2">รายการ</th>
            <th colspan="4" width=240>จำนวน</th>
            <th rowspan="2">หมายเหตุ</th>
        </tr>
        <tr>
            <th width=50>ขอเบิก</th>
            <th width=50>จ่าย</th>
            <th width=80>หน่วยนับ</th>
            <th width=60>ค้างจ่าย</th>
        </tr>
<?php 
$i=1;
foreach($model->items as $key => $v){
    echo "<tr><td align=center>".$i."</td>";
    echo "<td>".$v->list_item."</td>";
    echo "<td align=center>".$v->amount."</td>";
    echo "<td align=center>".$v->cut_off."</td>";
    echo "<td align=center>".$v->unit."</td>";
    echo "<td align=center>".$v->remain."</td>";
    echo "<td>&ensp;".$v->comment."</td></tr>";
    $i++;
}

?>
    </table>
</p>
<sethtmlpagefooter name="sign" value="on"/>
<htmlpagefooter name="sign">
<p>
    <table width=100%>
        <tr height=50>
            <td colspan=2></td>
        </tr>
        <tr>
            <td align="center" width=50%>
                (ลงชื่อ) &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ผู้ขอเบิก
            </td>
            <td align="center" width=50%>
                (ลงชื่อ) &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ผู้รับ
            </td>
        </tr>
        <tr>
            <td align="center">
                (<?= $model->bill_register?>)
            </td>
            <td align="center">
                (&emsp;&emsp;&emsp;&emsp; &emsp; &emsp; &emsp;)
            </td>
        </tr>
        <tr>
            <td align="center">
                ตำแหน่ง ...................................................
            </td>
            <td align="center">
                ตำแหน่ง ...................................................
            </td>
        </tr>
        <tr>
            <td align="center" width=50%>
                วันที่ ...../............./..........
            </td>
            <td align="center" width=50%>
                วันที่ ...../............./..........
            </td>
        </tr>
        <tr height=50>
            <td colspan=2>&emsp;</td>
        </tr>
        <tr>
            <td align="center" width=50%>
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            </td>
            <td align="center" width=50%>
                (ลงชื่อ) &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ผู้จ่าย
            </td>
        </tr>
        <tr>
            <td align="center">
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            </td>
            <td align="center">
            (นางวีรยา  อุ่นท้าว)
            </td>
        </tr>
        <tr>
            <td align="center">
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            </td>
            <td align="center">
            ตำแหน่ง นักวิชาการพัสดุ
            </td>
        </tr>
        <tr>
            <td align="center" width=50%>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            </td>
            <td align="center" width=50%>
                วันที่ ...../............./..........
            </td>
        </tr>
        <tr height=50>
            <td colspan=2>&emsp;</td>
        </tr>
        <tr>
            <td align="center" width=50%>
                (ลงชื่อ) &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ผู้ตรวจสอบ
            </td>
            <td align="center" width=50%>
                (ลงชื่อ) &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ผู้อนุมัติ
            </td>
        </tr>
        <tr>
            <td align="center">
            (นายวัชระ  พิมพาลัย)
            </td>
            <td align="center">
            (นางพัชรา  เดชาวัตร)
            </td>
        </tr>
        <tr>
            <td align="center">
            ตำแหน่ง นักวิชาการสาธารณสุขปฏิบัติการ
            </td>
            <td align="center">
            ตำแหน่ง นักจัดการงานทั่วไปชำนาญการ
            </td>
        </tr>
        <tr>
            <td align="center" width=50%>
                วันที่ ...../............./..........
            </td>
            <td align="center" width=50%>
                วันที่ ...../............./..........
            </td>
        </tr>
    </table>
</p>
</htmlpagefooter>