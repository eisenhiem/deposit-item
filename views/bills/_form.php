<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use unclead\widgets\MultipleInput;
//use unclead\widgets\MultipleInputColumn;
use unclead\multipleinput\MultipleInput;
use kartik\select2\Select2;
use yii\helpers\Url;


use app\models\Departments;
use app\models\Item;

$deps = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');
$item = ArrayHelper::map(Item::find()->where(['item_avaliable' => '1'])->all(), 'item_name', 'item_name');
/* @var $this yii\web\View */
/* @var $model app\models\bills */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bills-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'bill_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dep_id')->dropDownList([$deps, ['prompt'=>'เลือกกลุ่มงาน/ฝ่าย/แผนก']]) ?>

    <?php //= $form->field($model, 'bill_date')->textInput() ?>

    <?= $form->field($model, 'bill_register')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'items')->widget(MultipleInput::className(), [
    'max' => 20,
    'columns' => [
        [
            'name'  => 'list_item',
            'headerOptions' => ['style' => 'width:300px'],
            'type'  => Select2::className(),
            'title' => 'รายการ',
            'enableError' => true,
            'options' => [
                'data' => $item,
            ]
        ],
        [
            'name'  => 'amount',
            'type'  => 'textInput',
            'title' => 'จำนวน',
            'defaultValue' => 1,
            'headerOptions' => ['style' => 'width:60px'],
        ],
        [
            'name'  => 'unit',
            'type'  => 'textInput',
            'title' => 'หน่วยนับ',
            'headerOptions' => ['style' => 'width:100px'],
        ],
        [
            'name'  => 'comment',
            'type'  => 'textInput',
            'title' => 'หมายเหตุ',
        ]
    ]
 ]);
?>

    <?php // $form->field($model, 'prepare_id')->textInput() ?>

    <?php // $form->field($model, 'head_id')->textInput() ?>

    <?php // $form->field($model, 'auth_id')->textInput() ?>

    <?php // $form->field($model, 'bill_status_id')->dropDownList([ 5 => '5', 4 => '4', 3 => '3', 2 => '2', 1 => '1', 0 => '0', ], ['prompt' => '']) 
    ?>
<?php $this->registerJs("
    $('#".Html::getInputId($model, 'items')."').on('afterInit', function(){
            //console.log('calls on after initialization event');
        }).on('beforeAddRow', function(e) {
            //console.log('calls on before add row event');
        }).on('afterAddRow', function(e) {
            //console.log('calls on after add row event');
        }).on('beforeDeleteRow', function(e, item){
            //console.log(item);
            return confirm('คุณแน่ใจนะว่าต้องการลบ?'); 
        }).on('afterDeleteRow', function(e, item){
            //console.log(item);
            var id_case = item.find('.list-cell__block_no').find('input[id*=\"".Html::getInputId($model, 'items')."-\"]').val();
            var from_id = item.find('.list-cell__block_no').find('input[id*=\"".Html::getInputId($model, 'items')."-\"][id$=\"-from_id\"]').val();
            var im_id = item.find('.list-cell__block_no').find('input[id*=\"".Html::getInputId($model, 'items')."-\"][id$=\"-id\"]').val();
            //alert(id_case + from_id);
            $.post(\"".Url::to(['/immuno/staining/finance-service-charge-delete'])."\",
                {
                    id_case: id_case,
                    from_id: from_id,
                    im_id: im_id
                },
                function(data, status){
                    //alert(\"Data: \" + data); // + \"Status: \" + status
                }
            );
    });

")?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
