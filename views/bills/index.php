<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BillsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการใบคำขอเบิก';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bills-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php //= Html::a('เพิ่มใบคำขอเบิก', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'bill_id',
            'bill_no',
            //'dep_id',
            [
                'attribute'=>'dep_id',
                'label'=>'กลุ่มงาน/ฝ่าย/แผนก',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDepName();
                }
            ],

            'bill_date',
            'bill_register',
            //'prepare_id',
            //'head_id',
            //'auth_id',
            //'bill_status_id',
            [
                'attribute'=>'bill_status_id',
                'label'=>'สถานะรายการ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getStatusName();
                }
            ],

            /*
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{approve}',
                'buttons'=>[
                    'approve' => function($url,$model,$key){
                      return Html::a('อนุมัติ',['approve','id'=>$model->bill_id],['class' => 'btn btn-success']);
                    }
                ]
            ],
            */
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['view','id'=>$model->bill_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-warning'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Yii::$app->user->identity->id == '1' ? (Html::a('ตรวจสอบ',['lists/check','id'=>$model->bill_id],['class' => 'btn btn-warning'])):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-info'],
                'template'=>'{print}',
                'buttons'=>[
                    'print' => function($url,$model,$key){
                      return Yii::$app->user->identity->id == '1' ? (Html::a('พิมพ์ใบเบิก',['print','id'=>$model->bill_id],['class' => 'btn btn-info'])):'';
                    }
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
