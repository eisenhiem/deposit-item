<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

use app\models\Departments;
$deps = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $model app\models\BillsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bills-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="col-md-2">
    <?= $form->field($model, 'bill_no') ?>
    </div>

    <div class="col-md-4">
    <?= $form->field($model, 'dep_id')->dropDownList([$deps, ['prompt'=>'เลือกกลุ่มงาน/ฝ่าย/แผนก']])  ?>
    </div>

    <div class="col-md-3">
    <?= $form->field($model, 'bill_date')->widget(DatePicker::ClassName(),
    [
        'name' => 'bill_date', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ขอเบิก'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    </div>

    <div class="col-md-3">
    <?= $form->field($model, 'bill_register') ?>
    </div>

    <?php // echo $form->field($model, 'prepare_id') ?>

    <?php // echo $form->field($model, 'head_id') ?>

    <?php // echo $form->field($model, 'auth_id') ?>

    <?php // echo $form->field($model, 'bill_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
