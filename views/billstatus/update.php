<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BillStatus */

$this->title = 'Update Bill Status: ' . $model->bill_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Bill Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bill_status_id, 'url' => ['view', 'id' => $model->bill_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bill-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
