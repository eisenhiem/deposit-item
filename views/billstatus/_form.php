<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BillStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bill_status_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
