<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BillStatus */

$this->title = 'Create Bill Status';
$this->params['breadcrumbs'][] = ['label' => 'Bill Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
