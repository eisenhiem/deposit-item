<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'โปรแกรมเบิกพัสดุ';
?>
<div class="site-index">
<br>
<br>
<br>
<br>
    <div class="jumbotron">
        <h1>โปรแกรมเบิกพัสดุ Online</h1>

        <p class="lead">กดปุ่ม ด้านล่างเพื่อ สร้างใบคำขอเบิกพัสดุ</p>
        <p>
        <?= Html::a('เพิ่มใบคำขอเบิก', ['bills/create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>
