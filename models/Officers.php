<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "officers".
 *
 * @property int $officer_id
 * @property string|null $officer_name ชื่อ-สกุล
 * @property string|null $officer_position ตำแหน่ง
 * @property string|null $officer_level ระดับ
 */
class Officers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'officers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['officer_level'], 'string'],
            [['officer_name', 'officer_position'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'officer_id' => 'Officer ID',
            'officer_name' => 'ชื่อ-สกุล',
            'officer_position' => 'ตำแหน่ง',
            'officer_level' => 'ระดับ',
        ];
    }
}
