<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\bills;

/**
 * BillsSearch represents the model behind the search form of `app\models\bills`.
 */
class BillsSearch extends bills
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bill_id', 'dep_id', 'prepare_id', 'head_id', 'auth_id'], 'integer'],
            [['bill_no', 'bill_date', 'bill_register', 'bill_status_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = bills::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bill_id' => $this->bill_id,
            'dep_id' => $this->dep_id,
            'bill_date' => $this->bill_date,
            'prepare_id' => $this->prepare_id,
            'head_id' => $this->head_id,
            'auth_id' => $this->auth_id,
        ]);

        $query->andFilterWhere(['like', 'bill_no', $this->bill_no])
            ->andFilterWhere(['like', 'bill_register', $this->bill_register])
            ->andFilterWhere(['like', 'bill_status_id', $this->bill_status_id]);

        return $dataProvider;
    }
}
