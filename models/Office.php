<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property int $office_id
 * @property string|null $office_name
 * @property string|null $office_address
 * @property string|null $office_postcode
 * @property string|null $office_contact
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['office_name', 'office_address', 'office_contact'], 'string', 'max' => 255],
            [['office_postcode'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'office_id' => 'Office ID',
            'office_name' => 'Office Name',
            'office_address' => 'Office Address',
            'office_postcode' => 'Office Postcode',
            'office_contact' => 'Office Contact',
        ];
    }
}
