<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property int $item_id
 * @property string|null $item_name
 * @property string|null $item_type
 * @property string|null $item_unit
 * @property int|null $item_stock
 * @property string|null $item_incharge
 * @property string|null $item_avaliable
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_stock'], 'integer'],
            [['item_name', 'item_type', 'item_unit', 'item_incharge'], 'string', 'max' => 255],
            [['item_avaliable'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'item_name' => 'ชื่อรายการ',
            'item_type' => 'ประเภท',
            'item_unit' => 'หน่วย',
            'item_stock' => 'จำนวนคงเหลือ',
            'item_incharge' => 'ผู้รับผิดชอบ',
            'item_avaliable' => 'สถานะรายการ',
        ];
    }
}
