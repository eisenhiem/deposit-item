<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lists".
 *
 * @property int $list_id
 * @property int|null $bill_id
 * @property string|null $list_item รายการ
 * @property int|null $amount จำนวนขอเบิก
 * @property int|null $cut_off ตัดจ่าย
 * @property string|null $cut_off_date วันที่ตัดจ่าย
 * @property int|null $remain ค้างจ่าย
 * @property string|null $comment หมายเหตุ
 *
 * @property Bills $bill
 */
class Lists extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bill_id', 'amount', 'cut_off', 'remain'], 'integer'],
            [['cut_off_date'], 'safe'],
            [['comment'], 'string'],
            [['unit'], 'string', 'max' => 30],
            [['list_item'], 'string', 'max' => 255],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bills::className(), 'targetAttribute' => ['bill_id' => 'bill_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'bill_id' => 'Bill ID',
            'list_item' => 'รายการ',
            'amount' => 'จำนวนขอเบิก',
            'cut_off' => 'ตัดจ่าย',
            'cut_off_date' => 'วันที่ตัดจ่าย',
            'remain' => 'ค้างจ่าย',
            'comment' => 'หมายเหตุ',
            'unit' => 'หน่วยนับ',
        ];
    }

    /**
     * Gets query for [[Bill]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bills::className(), ['bill_id' => 'bill_id']);
    }
}
