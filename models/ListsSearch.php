<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lists;

/**
 * ListsSearch represents the model behind the search form of `app\models\Lists`.
 */
class ListsSearch extends Lists
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_id', 'bill_id', 'amount', 'cut_off', 'remain'], 'integer'],
            [['list_item', 'unit','cut_off_date', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lists::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'list_id' => $this->list_id,
            'bill_id' => $this->bill_id,
            'amount' => $this->amount,
            'cut_off' => $this->cut_off,
            'cut_off_date' => $this->cut_off_date,
            'remain' => $this->remain,
        ]);

        $query->andFilterWhere(['like', 'list_item', $this->list_item])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
