<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bills".
 *
 * @property int $bill_id
 * @property string|null $bill_no เลขที่ใบเบิก
 * @property int|null $dep_id แผนก/ฝ่ายที่ขอเบิก
 * @property string|null $bill_date วันที่ขอเบิก
 * @property string|null $bill_register ผู้ขอเบิก
 * @property int|null $prepare_id ผู้จ่ายพัสดุ
 * @property int|null $head_id หัวหน้าพัสดุ
 * @property int|null $auth_id ผู้อนุมัติ
 * @property string|null $bill_status_id สถานะใบคำขอ
 *
 * @property Lists[] $lists
 */
class Bills extends \yii\db\ActiveRecord
{

    public $items;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bills';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id', 'prepare_id', 'head_id', 'auth_id'], 'integer'],
            [['bill_date','items'], 'safe'],
            [['bill_status_id'], 'string'],
            [['bill_no', 'bill_register'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bill_id' => 'Bill ID',
            'bill_no' => 'เลขที่ใบเบิก',
            'dep_id' => 'แผนก/ฝ่ายที่ขอเบิก',
            'bill_date' => 'วันที่ขอเบิก',
            'bill_register' => 'ผู้ขอเบิก',
            'prepare_id' => 'ผู้จ่ายพัสดุ',
            'head_id' => 'หัวหน้าพัสดุ',
            'auth_id' => 'ผู้อนุมัติ',
            'bill_status_id' => 'สถานะใบคำขอ',
            'items' => 'รายการที่ขอเบิก',
        ];
    }

    /**
     * Gets query for [[Lists]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLists()
    {
        return $this->hasMany(Lists::className(), ['bill_id' => 'bill_id']);
    }
    
    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(BillStatus::className(), ['bill_status_id' => 'bill_status_id']);
    }
    public function getStatusName()
    {
        return $this->status->status_name;
    }

    /**
     * Gets query for [[Department]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['dep_id' => 'dep_id']);
    }
    public function getDepName()
    {
        return $this->department->dep_name;
    }

    /**
     * Gets query for [[Officer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfficer()
    {
        return $this->hasOne(Officers::className(), ['officer_id' => 'prepare_id']);
    }
    public function getOfficerName()
    {
        return $this->officer->officer_name;
    }
    public function getOfficerPos()
    {
        return $this->officer->officer_position;
    }

    /**
     * Gets query for [[Header]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHead()
    {
        return $this->hasOne(Officers::className(), ['officer_id' => 'head_id']);
    }
    public function getHeadName()
    {
        return $this->head->officer_name;
    }
    public function getHeadPos()
    {
        return $this->head->officer_position;
    }
    /**
     * Gets query for [[Authorize]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuth()
    {
        return $this->hasOne(Officers::className(), ['officer_id' => 'auth_id']);
    }
    public function getAuthName()
    {
        return $this->auth->officer_name;
    }
    public function getAuthPos()
    {
        return $this->auth->officer_position;
    }

}
