<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bill_status".
 *
 * @property string $bill_status_id
 * @property string|null $status_name
 */
class BillStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bill_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bill_status_id'], 'required'],
            [['bill_status_id'], 'string', 'max' => 1],
            [['status_name'], 'string', 'max' => 255],
            [['bill_status_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bill_status_id' => 'Bill Status ID',
            'status_name' => 'Status Name',
        ];
    }
}
