/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : supply

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2020-10-28 11:21:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bill_status`
-- ----------------------------
DROP TABLE IF EXISTS `bill_status`;
CREATE TABLE `bill_status` (
`bill_status_id`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`status_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`bill_status_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of bill_status
-- ----------------------------
BEGIN;
INSERT INTO `bill_status` VALUES ('0', 'ส่งใบคำขอ'), ('1', 'ตรวจสอบรายการ'), ('2', 'อนุมัติ'), ('3', 'จ่าย'), ('4', 'รับ'), ('5', 'จัดทำเอกสาร');
COMMIT;

-- ----------------------------
-- Table structure for `bills`
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills` (
`bill_id`  int(11) NOT NULL AUTO_INCREMENT ,
`bill_no`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'เลขที่ใบเบิก' ,
`dep_id`  int(11) NULL DEFAULT NULL COMMENT 'แผนก/ฝ่ายที่ขอเบิก' ,
`bill_date`  date NULL DEFAULT NULL COMMENT 'วันที่ขอเบิก' ,
`bill_register`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ผู้ขอเบิก' ,
`prepare_id`  int(11) NULL DEFAULT NULL COMMENT 'ผู้จ่ายพัสดุ' ,
`head_id`  int(11) NULL DEFAULT NULL COMMENT 'หัวหน้าพัสดุ' ,
`auth_id`  int(11) NULL DEFAULT NULL COMMENT 'ผู้อนุมัติ' ,
`bill_status_id`  enum('5','4','3','2','1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'สถานะใบคำขอ' ,
PRIMARY KEY (`bill_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of bills
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
`dep_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`dep_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'แผนก/ฝ่าย' ,
PRIMARY KEY (`dep_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=16

;

-- ----------------------------
-- Records of departments
-- ----------------------------
BEGIN;
INSERT INTO `departments` VALUES ('1', 'กลุ่มงานบริหารทั่วไป'), ('2', 'กลุ่มงานยุทธศาสตร์'), ('3', 'กลุ่มงานประกันสุขภาพ'), ('4', 'องค์กรแพทย์'), ('5', 'กลุ่มงานปฐมภูมิ'), ('6', 'แผนกอุบัติเหตุ-ฉุกเฉิน'), ('7', 'แผนกผู้ป่วยนอก'), ('8', 'แผนกผู้ป่วยใน'), ('9', 'แผนกเวชระเบียนและห้องบัตร'), ('10', 'กลุ่มงานเภสัชกรรม'), ('11', 'แผนกซักฟอกและจ่ายกลาง'), ('12', 'กลุ่มงานแพทย์แผนไทย'), ('13', 'กลุ่มงานทันตกรรม'), ('14', 'กลุ่มงานเทคนิกบริการทางการแพทย์'), ('15', 'กลุ่มงานกายภาพบำบัด');
COMMIT;

-- ----------------------------
-- Table structure for `lists`
-- ----------------------------
DROP TABLE IF EXISTS `lists`;
CREATE TABLE `lists` (
`list_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`bill_id`  int(11) NULL DEFAULT NULL ,
`list_item`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'รายการ' ,
`amount`  int(11) NULL DEFAULT NULL COMMENT 'จำนวนขอเบิก' ,
`cut_off`  int(11) NULL DEFAULT NULL COMMENT 'ตัดจ่าย' ,
`cut_off_date`  date NULL DEFAULT NULL COMMENT 'วันที่ตัดจ่าย' ,
`remain`  int(11) NULL DEFAULT NULL COMMENT 'ค้างจ่าย' ,
`comment`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'หมายเหตุ' ,
PRIMARY KEY (`list_id`),
FOREIGN KEY (`bill_id`) REFERENCES `bills` (`bill_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
INDEX `bill_id` (`bill_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of lists
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `office`
-- ----------------------------
DROP TABLE IF EXISTS `office`;
CREATE TABLE `office` (
`office_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`office_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_address`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_postcode`  varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`office_contact`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`office_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of office
-- ----------------------------
BEGIN;
INSERT INTO `office` VALUES ('1', 'โรงพยาบาลเหล่าเสือโก้ก', '298 หมู่ 6 ต.เหล่าเสือโก้ก อ.เหล่าเสือโก้ก จ.อุบลราชธานี', '34000', '(045)-304205,(045)-304265');
COMMIT;

-- ----------------------------
-- Table structure for `officers`
-- ----------------------------
DROP TABLE IF EXISTS `officers`;
CREATE TABLE `officers` (
`officer_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`officer_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ชื่อ-สกุล' ,
`officer_position`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ตำแหน่ง' ,
`officer_level`  enum('3','2','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ระดับ' ,
`active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT 'สถานะเจ้าหน้าที่' ,
PRIMARY KEY (`officer_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of officers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for `bills`
-- ----------------------------
ALTER TABLE `bills` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `departments`
-- ----------------------------
ALTER TABLE `departments` AUTO_INCREMENT=16;

-- ----------------------------
-- Auto increment value for `lists`
-- ----------------------------
ALTER TABLE `lists` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `office`
-- ----------------------------
ALTER TABLE `office` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `officers`
-- ----------------------------
ALTER TABLE `officers` AUTO_INCREMENT=1;
