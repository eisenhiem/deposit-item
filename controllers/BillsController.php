<?php

namespace app\controllers;

use Yii;
use app\models\bills;
use app\models\BillsSearch;
use app\models\Officers;
use app\models\Office;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use app\models\Lists;
use app\models\ListsSearch;
use yii\base\Exception;


/**
 * BillsController implements the CRUD actions for bills model.
 */
class BillsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all bills models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['bill_id'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single bills model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new ListsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['bill_id' => $id])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new bills model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // ส้างใบคำขอเบิก 
        $model = new bills();

        if ($model->load(Yii::$app->request->post())) {
            // สร้าง transection 
            $transaction = Yii::$app->db->beginTransaction();
            // หาค่า bill no ล่าสุด
            $max_bill_no = Bills::find()->select('max(bill_no) as max_no')->scalar();
            // ถ้าหาเลข bill no ไม่พบ ให้ตั้งค่าเป็น 1
            if(!$max_bill_no){
                $max_bill_no = 1;
            }
            try {
                // บันทึกใบคำขอ
                $model->head_id = Officers::find()->select('officer_id')->where(['officer_level'=>'2','active'=>'1'])->limit(1);
                $model->bill_date = date('Y-m-d');
                $model->bill_no = $max_bill_no+1;
                $model->save(false); 
                // ประกาศรับค่าจาก Post
                $items = Yii::$app->request->post();

                //นำรายการขอเบิกที่เลือกมา loop บันทึก
                foreach($items['Bills']['items'] as $key => $val){ 
                    if(empty($val['list_id'])){
                        $lists = new Lists();
                    }else{
                        $lists = Lists::findOne($val['list_id']);
                    }
                    $lists->bill_id = $model->bill_id;
                    $lists->list_item = $val['list_item'];
                    $lists->amount = $val['amount'];   
                    $lists->unit = $val['unit'];                 
                    $lists->comment = $val['comment'];
                    $lists->save();
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', 'บันทึกข้อมูลเรียบร้อย');
                return $this->redirect(['index']);

            } catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'มีข้อผิดพลาดในการบันทึก');
                return $this->redirect(['index']);
            }

            return $this->redirect(['view', 'id' => $model->bill_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing bills model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->items = Lists::find()->where(['bill_id' => $model->bill_id])->all();
        
        if ($model->load(Yii::$app->request->post())) {
            // สร้าง transection 
            $transaction = Yii::$app->db->beginTransaction();

            try {
                // บันทึกใบคำขอ
                $model->save(); 
                // ประกาศรับค่าจาก Post
                $items = Yii::$app->request->post();

                //นำรายการขอเบิกที่เลือกมา loop บันทึก
                foreach($items['Bills']['items'] as $key => $val){ 
                    if(empty($val['list_id'])){
                        $lists = new Lists();
                    }else{
                        $lists = Lists::findOne($val['list_id']);
                    }
                    $lists->bill_id = $model->bill_id;
                    $lists->list_item = $val['list_item'];
                    $lists->amount = $val['amount'];                    
                    $lists->unit = $val['unit'];                 
                    $lists->comment = $val['comment'];
                    $lists->save();
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', 'บันทึกข้อมูลเรียบร้อย');
                return $this->redirect(['index']);

            } catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'มีข้อผิดพลาดในการบันทึก');
                return $this->redirect(['index']);
            }
            return $this->redirect(['view', 'id' => $model->bill_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing bills model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

        /**
     * Updates an existing bills model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->items = Lists::find()->where(['bill_id' => $model->bill_id])->all();
        
        $model->auth_id = Yii::$app->user->identity->id;
        $model->bill_status_id = '2';
        $model->save(false);

        return $this->redirect(['index']);
    }

        /**
     * Lists all repairs models.
     * @return mixed
     */
    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $model->items = Lists::find()->where(['bill_id' => $model->bill_id])->all();        
        $office = Office::find()->one();

        $content = $this->renderPartial('_print', [
            'model' => $model,
            'office' => $office,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => '.[ใบคำขอเบิกที่ : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Finds the bills model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return bills the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = bills::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
